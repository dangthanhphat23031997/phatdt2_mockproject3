﻿using Microsoft.EntityFrameworkCore;
using OnlineShoppingWebApplication.Core.Data;
using OnlineShoppingWebApplication.Core.Entities;
using OnlineShoppingWebApplication.Core.Repositories.Base;
using OnlineShoppingWebApplication.ViewModels.Paging;
using OnlineShoppingWebApplication.ViewModels.Paging.Category;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineShoppingWebApplication.Core.Repositories
{
    public class CategoryRepository : BaseRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(OnlineShoppingWebApplicationDbContext context) : base(context)
        {
        }

        public async Task<Category> GetByName(string name)
        {
            return await _entities.AsNoTracking().FirstOrDefaultAsync(x => x.Name == name);
        }

        public async Task<PagedResult<Category>> GetCategoriesPaging(GetCategoryPagingRequest request)
        {
            var categories = _entities.AsQueryable();
            if (!string.IsNullOrWhiteSpace(request.Keyword))
            {
                categories = categories.Where(x => x.Name.Contains(request.Keyword));
            }

            int totalRecords = await categories.CountAsync();

            var data = await categories.Skip((request.PageIndex -1) * request.PageSize).Take(request.PageSize).ToListAsync();

            var result = new PagedResult<Category>()
            {
                PageIndex = request.PageIndex,
                PageSize = request.PageSize,
                TotalRecords = totalRecords,
                Items = data
            };
            return result;
        }
    }
}
