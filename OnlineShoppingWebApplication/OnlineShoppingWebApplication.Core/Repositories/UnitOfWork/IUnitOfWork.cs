﻿using System;
using System.Threading.Tasks;

namespace OnlineShoppingWebApplication.Core.Repositories.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        ICategoryRepository CategoryRepository { get; }
        IProductRepository ProductRepository { get; }
        Task DisposeAsync();
        bool SaveChanges();
        Task<bool> SaveChangesAsync();
    }
}
