﻿using Microsoft.AspNetCore.Identity;
using System;

namespace OnlineShoppingWebApplication.Core.Entities
{
    public class BaseUser : IdentityUser<Guid>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DOB { get; set; }
    }
}
