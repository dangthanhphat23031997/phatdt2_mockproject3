﻿using Microsoft.AspNetCore.Identity;
using System;

namespace OnlineShoppingWebApplication.Core.Entities
{
    public class BaseRole : IdentityRole<Guid>
    {
        public string Description { get; set; }
    }
}
