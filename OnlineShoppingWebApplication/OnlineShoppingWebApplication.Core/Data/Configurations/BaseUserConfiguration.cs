﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OnlineShoppingWebApplication.Core.Entities;

namespace OnlineShoppingWebApplication.Core.Data.Configurations
{
    public class BaseUserConfiguration : IEntityTypeConfiguration<BaseUser>
    {
        public void Configure(EntityTypeBuilder<BaseUser> builder)
        {
            builder.ToTable("User");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.FirstName)
                .HasMaxLength(200)
                .IsRequired(true);

            builder.Property(x => x.LastName)
                .HasMaxLength(200)
                .IsRequired(true);

            builder.Property(x => x.DOB)
                .IsRequired(false);
        }
    }
}
