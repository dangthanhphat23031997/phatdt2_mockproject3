﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OnlineShoppingWebApplication.Core.Entities;

namespace OnlineShoppingWebApplication.Core.Data.Configurations
{
    public class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name).HasMaxLength(500).IsRequired(true).IsUnicode(true);
            builder.Property(x => x.Description).HasMaxLength(1000).IsRequired(false).IsUnicode(true);
            builder.Property(x => x.Price).IsRequired(true);

            builder.HasOne(x => x.Category)
                .WithMany(x => x.Products)
                .HasForeignKey(x => x.CategoryId)
                .HasConstraintName("FK_Category_Products")
                .OnDelete(DeleteBehavior.ClientSetNull);
        }
    }
}
