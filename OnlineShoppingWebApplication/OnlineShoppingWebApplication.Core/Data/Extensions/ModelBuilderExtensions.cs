﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using OnlineShoppingWebApplication.Core.Entities;
using System;
using System.Collections.Generic;

namespace OnlineShoppingWebApplication.Core.Data.Extensions
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder builder)
        {
            IList<Category> categoriesList = new List<Category>();
            Category category = new Category()
            {

                Id = Guid.NewGuid(),
                Name = "Shoes",
                Description = "Shoes"
            };

            Category shirt = new Category()
            {
                Id = Guid.NewGuid(),
                Name = "Shirt",
                Description = "Shirt"
            };

            categoriesList.Add(category);
            categoriesList.Add(shirt);
            builder.Entity<Category>().HasData(categoriesList);

            builder.Entity<Product>().HasData(
                new Product()
                {
                    Id = Guid.NewGuid(),
                    CategoryId = category.Id,
                    Name = "Adidas",
                    Description = "Adidas",
                    Price = 100
                });

            var hasher = new PasswordHasher<BaseUser>();
            IList<BaseUser> userList = new List<BaseUser>();
            BaseUser user_1 = new BaseUser()
            {
                Id = Guid.NewGuid(),
                UserName = "admin",
                NormalizedUserName = "ADMIN",
                FirstName = "Admin",
                LastName = "01",
                Email = "admin01@gmail.com",
                NormalizedEmail = "ADMIN01@GMAIL.COM",
                EmailConfirmed = true,
                DOB = new DateTime(1997, 03, 23)
            };
            user_1.PasswordHash = hasher.HashPassword(user_1, "123qwE@");
            userList.Add(user_1);

            BaseUser user_2 = new BaseUser()
            {
                Id = Guid.NewGuid(),
                UserName = "user",
                NormalizedUserName = "USER",
                FirstName = "User",
                LastName = "01",
                Email = "user01@gmail.com",
                NormalizedEmail = "USER01@GMAIL.COM",
                EmailConfirmed = true,
                DOB = new DateTime(1997, 03, 23)
            };
            user_2.PasswordHash = hasher.HashPassword(user_2, "123qwE@");
            userList.Add(user_2);

            builder.Entity<BaseUser>().HasData(userList);

            var roleIdFirst = Guid.NewGuid();
            var roleIdSecond = Guid.NewGuid();
            builder.Entity<BaseRole>().HasData(
                new BaseRole()
                {
                    Id = roleIdFirst,
                    Name = "Admin",
                    NormalizedName = "ADMIN",
                    Description = "Description admin"
                },
                new BaseRole()
                {
                    Id = roleIdSecond,
                    Name = "User",
                    NormalizedName = "USER",
                    Description = "Description user"
                }
            );

            builder.Entity<IdentityUserRole<Guid>>().HasData(
                new IdentityUserRole<Guid>()
                {
                    UserId = user_1.Id,
                    RoleId = roleIdFirst
                },
                new IdentityUserRole<Guid>()
                {
                    UserId = user_2.Id,
                    RoleId = roleIdSecond
                }
            );
        }
    }
}
