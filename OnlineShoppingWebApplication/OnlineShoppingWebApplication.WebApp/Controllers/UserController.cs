﻿using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OnlineShoppingWebApplication.Service.Common;
using OnlineShoppingWebApplication.Service.Services.User;
using OnlineShoppingWebApplication.Service.Services.User.DTOs;
using OnlineShoppingWebApplication.ViewModels.Paging.User;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineShoppingWebApplication.WebApp.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly INotyfService _notyfService;
        public UserController(IUserService userService, INotyfService notyfService)
        {
            _userService = userService;
            _notyfService = notyfService;
        }

        [HttpGet]
        public IActionResult SignIn()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> SignIn(UserLoginDto request)
        {
            if (!ModelState.IsValid)
            {
                return View(request);
            }

            var authen = await _userService.Authentication(request);

            if (authen.IsSuccessed)
            {
                var userPrincipal = _userService.ValidateToken(authen.Message);
                var authProperties = new AuthenticationProperties
                {
                    ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(30),
                    IsPersistent = false
                };
                HttpContext.Session.SetString(Constants.Token, authen.Message);
                await HttpContext.SignInAsync(
                            CookieAuthenticationDefaults.AuthenticationScheme,
                            userPrincipal,
                            authProperties);

                return RedirectToAction("Index", "Home");
            }

            _notyfService.Error(authen.Message);
            return View();
        }

        [HttpGet]
        public IActionResult SignUp()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> SignUp(UserCreateDto dto)
        {
            if (!ModelState.IsValid)
            {
                return View(dto);
            }

            var user = await _userService.Create(dto);
            if (user.IsSuccessed)
            {
                _notyfService.Success(user.Message);
                UserLoginDto request = new UserLoginDto()
                {
                    UserName = dto.UserName,
                    Password = dto.Password
                };

                var authen = await _userService.Authentication(request);

                if (authen.IsSuccessed)
                {
                    var userPrincipal = _userService.ValidateToken(authen.Message);
                    var authProperties = new AuthenticationProperties
                    {
                        ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(4),
                        IsPersistent = false
                    };
                    HttpContext.Session.SetString(Constants.Token, authen.Message);
                    await HttpContext.SignInAsync(
                                CookieAuthenticationDefaults.AuthenticationScheme,
                                userPrincipal,
                                authProperties);

                    return RedirectToAction("Index", "Home");
                }
            }

            _notyfService.Error(user.Message);

            return View();
        }

        [Authorize]
        public async Task<IActionResult> LogOut()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("SignIn", "User");
        }

        [Authorize]
        public async Task<IActionResult> AccountInformation()
        {
            var claims = HttpContext.User.Claims.ToList();
            var userName = claims.FirstOrDefault(x => x.Type == Constants.UserName).Value;
            var user = await _userService.GetByUserName(userName);
            if(user.IsSuccessed == false)
            {
                _notyfService.Error(user.Message);
            }

            return View(user.ResultObj);
        }

        [Authorize]
        [HttpGet]
        public IActionResult ChangePassword()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> ChangePassword(UserChangePasswordDto dto)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var claims = HttpContext.User.Claims.ToList();
            dto.UserName = claims.FirstOrDefault(x => x.Type == Constants.UserName).Value;
            var user = await _userService.ChangePassword(dto);
            if (user.IsSuccessed)
            {
                _notyfService.Success(user.Message);
                return View();
            }

            _notyfService.Error(user.Message);
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Edit(Guid id)
        {
            var result = await _userService.GetById(id);
            if(result.IsSuccessed == false)
            {
                _notyfService.Error(result.Message);
                return View();
            }

            var user = result.ResultObj;
            UserEditDto userEdit = new UserEditDto();
            userEdit.Id = user.Id;
            userEdit.FirstName = user.FirstName;
            userEdit.LastName = user.LastName;
            userEdit.DOB = user.DOB;
            userEdit.Email = user.Email;
            userEdit.PhoneNumber = user.PhoneNumber;

            return View(userEdit);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(UserEditDto dto)
        {
            if (!ModelState.IsValid)
            {
                return View(dto);
            }

            var user = await _userService.Edit(dto);
            if (user.IsSuccessed)
            {
                _notyfService.Success(user.Message);
                return RedirectToAction("AccountInformation");
            }

            _notyfService.Error(user.Message);
            return View(dto);
        }


        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index(string keyword, int pageIndex = 1, int pageSize = 3)
        {
            GetUserPagingRequest request = new GetUserPagingRequest()
            {
                Keyword = keyword,
                PageSize = pageSize,
                PageIndex = pageIndex
            };

            ViewBag.Keyword = request.Keyword;

            var users = await _userService.GetUsersPaging(request);
            return View(users.ResultObj);
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Details(Guid id)
        {
            var user = await _userService.GetById(id);
            if (user.IsSuccessed)
            {
                return View(user.ResultObj);
            }

            _notyfService.Error(user.Message);
            return View();
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> Delete(Guid id)
        {
            var user = await _userService.DeleteById(id);
            if (user.IsSuccessed)
            {
                _notyfService.Success(user.Message);
                return RedirectToAction("Index");
            }

            _notyfService.Error(user.Message);
            return RedirectToAction("Index");
        }

        public IActionResult Forbidden()
        {
            return View();
        }
    }
}
