﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
var sidenav = $('.sidenav');
var sidenavCol = $('.sidenav-col');
var logoText = $('.logo-text');
var btnCloseSidenav = $('.btn-close');
var menuText = $('.sidenav-link-text');
var sidenav_nav = $('.sidenav-main');
var head_content = $('.head-content');
var head_content_collapsed = $('.head-content-collapsed');

function btnClose() {
    if (sidenav.hasClass('sidenav-collapsed')) {
        btnCloseSidenav.addClass('rotate-callapsed');
        btnCloseSidenav.removeClass('rotate');
    }
    else {
        btnCloseSidenav.addClass('rotate');
        btnCloseSidenav.removeClass('rotate-callapsed');
    }
}

function rightMenu() {
    //right menu
    if (sidenav.hasClass('sidenav-collapsed')) {
        sidenav_nav.addClass('sidenav-nav-collapsed');
        sidenav_nav.removeClass('sidenav-content');
        head_content.addClass('head-content-collapsed');
        head_content_collapsed.removeClass('head-content');
    }
    else {
        sidenav_nav.addClass('sidenav-content');
        sidenav_nav.removeClass('sidenav-nav-collapsed');
        head_content_collapsed.addClass('head-content');
        head_content.removeClass('head-content-collapsed');
    }
}

function closeSidenav() {
    // left menu
    sidenav.addClass('sidenav-collapsed');

    sidenavCol.addClass('sidenav-collapsed');

    logoText.addClass('hidden');

    btnClose();

    menuText.addClass('hidden');

    rightMenu();
}

function toggleSidenav() {
    // left menu
    var width = window.innerWidth;
    if (width < 1366) {
        return;
    }

    sidenav.toggleClass('sidenav-collapsed');

    sidenavCol.toggleClass('sidenav-collapsed');

    logoText.toggleClass('hidden');

    btnClose();

    menuText.toggleClass('hidden');

    //right menu
    rightMenu();
}

$(window).resize(function () {
    //debugger;
    var width = window.innerWidth;
    if (width < 1366) {
        closeSidenav();
    }
    else {
        toggleSidenav();
    }
});

$(function () {
    var width = window.innerWidth;
    if (width < 1366) {
        closeSidenav();
    }
    else {
        sidenav.removeClass('sidenav-collapsed');

        sidenavCol.removeClass('sidenav-collapsed');

        logoText.removeClass('hidden');

        btnClose();

        menuText.removeClass('hidden');

        rightMenu();
    }
});

$('#category').on('click', '#btnDelete', function () {
    //debugger;
    deleteItem($(this), 'category');
});

$('#product').on('click', '#btnDelete', function () {
    //debugger;
    deleteItem($(this), 'product');
});

$('#user').on('click', '#btnDelete', function () {
    //debugger;
    deleteItem($(this), 'user');
});

function deleteItem(item, controller) {
    //debugger;
    var id = item.val();
    var name = item.parent().parent()[0].children['name'].innerText;
    document.querySelector("#exampleModalCenter > form > input[name='id']").value = id;
    document.querySelector("#content-cofirm").innerText = `Are you sure you want to delete ${name}?`;
    document.querySelector("#exampleModalCenter > form").action = `/${controller}/delete`;
    $('#exampleModalCenter').modal('show');
}