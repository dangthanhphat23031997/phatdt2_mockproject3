﻿$(function () {
    var url = window.location.href;
    if (url.includes('/User/SignUp')) {
        $('#tab-link-text-signin').removeClass('active');
        $('#tab-link-text-signup').addClass('active');
    }
    else {
        $('#tab-link-text-signin').addClass('active');
        $('#tab-link-text-signup').removeClass('active');
    }
})