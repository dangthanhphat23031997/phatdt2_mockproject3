﻿using System;
using System.ComponentModel.DataAnnotations;

namespace OnlineShoppingWebApplication.Service.Services.DTOs
{
    public class ProductCreateDto
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public decimal? Price { get; set; }
        [Required(ErrorMessage = "The Category field is required.")]
        public Guid? CategoryId { get; set; }
    }
}
