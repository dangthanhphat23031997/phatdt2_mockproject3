﻿using OnlineShoppingWebApplication.Service.Common;
using OnlineShoppingWebApplication.Service.Services.DTOs;
using OnlineShoppingWebApplication.ViewModels.Paging;
using OnlineShoppingWebApplication.ViewModels.Paging.Category;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OnlineShoppingWebApplication.Service.Services
{
    public interface ICategoryService
    {
        Task<ApiResult<bool>> Create(CategoryCreateDto dto);
        Task<ApiResult<bool>> Edit(CategoryDto dto);
        Task<ApiResult<bool>> DeleteByName(string name);
        Task<ApiResult<CategoryDto>> GetByName(string name);
        Task<IList<CategoryDto>> GetAll();
        Task<ApiResult<CategoryDto>> GetById(Guid id);
        Task<ApiResult<PagedResult<CategoryDto>>> GetCategoriesPaging(GetCategoryPagingRequest request);
        Task<ApiResult<bool>> DeleteById(Guid id);
    }
}
