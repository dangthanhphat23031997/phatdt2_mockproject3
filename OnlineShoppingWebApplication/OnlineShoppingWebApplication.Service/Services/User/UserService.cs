﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using OnlineShoppingWebApplication.Core.Entities;
using OnlineShoppingWebApplication.Service.Common;
using OnlineShoppingWebApplication.Service.Services.User.DTOs;
using OnlineShoppingWebApplication.ViewModels.Paging;
using OnlineShoppingWebApplication.ViewModels.Paging.User;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using static OnlineShoppingWebApplication.Service.Common.Constants;

namespace OnlineShoppingWebApplication.Service.Services.User
{
    public class UserService : IUserService
    {
        private readonly UserManager<BaseUser> _userManager;
        private readonly SignInManager<BaseUser> _signInManager;
        private readonly IMapper _mapper;
        public UserService(UserManager<BaseUser> userManager, SignInManager<BaseUser> signInManager
            , IMapper mapper)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _mapper = mapper;
        }

        public async Task<ApiResult<bool>> Create(UserCreateDto dto)
        {
            if (!string.IsNullOrWhiteSpace(dto.Email))
            {
                var email = await _userManager.FindByEmailAsync(dto.Email);
                if (email != null)
                {
                    return new ApiErrorResult<bool>($"Email [{dto.Email}] is existing");
                }
            }

            var user = _mapper.Map<BaseUser>(dto);
            try
            {
                var result = await _userManager.CreateAsync(user, dto.Password);
                if (result.Succeeded == true)
                {
                    return new ApiSuccessResult<bool>($"User [{dto.UserName}] create success");
                }
                else
                {
                    return new ApiErrorResult<bool>(result.Errors.ToList().FirstOrDefault().Code);
                }
            }
            catch
            {
                return new ApiErrorResult<bool>($"User [{dto.UserName}] create failed");
            }
        }

        public async Task<ApiResult<bool>> Edit(UserEditDto dto)
        {
            BaseUser userEnt = null;
            if (!string.IsNullOrWhiteSpace(dto.Email))
            {
                userEnt = await _userManager.FindByEmailAsync(dto.Email);
                if (userEnt != null && userEnt.Id != dto.Id)
                {
                    return new ApiErrorResult<bool>($"Email [{dto.Email}] is existing");
                }
            }

            try
            {
                if(userEnt == null)
                {
                    userEnt = await _userManager.FindByIdAsync(dto.Id.ToString());
                    if (userEnt == null)
                    {
                        return new ApiErrorResult<bool>("Cannot found");
                    }
                }
                var user = _mapper.Map<UserEditDto, BaseUser>(dto, userEnt);
                var result = await _userManager.UpdateAsync(user);
                if (result.Succeeded == true)
                {
                    return new ApiSuccessResult<bool>($"User update success");
                }
                else
                {
                    return new ApiErrorResult<bool>(result.Errors.ToList().FirstOrDefault().Code);
                }
            }
            catch
            {
                return new ApiErrorResult<bool>($"User update failed");
            }
        }

        public async Task<IList<UserDto>> GetAll()
        {
            var users = await _userManager.Users.ToListAsync();
            var userDtos = _mapper.Map<IList<UserDto>>(users);
            return userDtos;
        }

        public async Task<ApiResult<UserDto>> GetByUserName(string userName)
        {
            var user = await _userManager.FindByNameAsync(userName);
            if(user == null)
            {
                return new ApiErrorResult<UserDto>($"Can not found [{userName}]");
            }

            var userDto = _mapper.Map<UserDto>(user);
            return new ApiSuccessResult<UserDto>(userDto);
        }

        public async Task<ApiResult<UserDto>> GetById(Guid id)
        {
            var user = await _userManager.FindByIdAsync(id.ToString());
            if (user == null)
            {
                return new ApiErrorResult<UserDto>($"Can not found.");
            }

            var userDto = _mapper.Map<UserDto>(user);
            return new ApiSuccessResult<UserDto>(userDto);
        }

        public async Task<ApiResult<bool>> DeleteById(Guid id)
        {
            var user = await _userManager.FindByIdAsync(id.ToString());
            if (user == null)
            {
                return new ApiErrorResult<bool>($"Can not found.");
            }

            var del = await _userManager.DeleteAsync(user);
            if (del.Succeeded)
            {
                return new ApiSuccessResult<bool>($"[{user.UserName}] deleted successfully");
            }

            return new ApiErrorResult<bool>(del.Errors.FirstOrDefault().Code);
        }

        public async Task<ApiResult<string>> Authentication(UserLoginDto dto)
        {
            var user = await _userManager.FindByNameAsync(dto.UserName);
            if (user != null && await _userManager.CheckPasswordAsync(user, dto.Password))
            {
                await _signInManager.SignInAsync(user, true);
                var signin = await _signInManager.PasswordSignInAsync(user, dto.Password, dto.RememberPassword, true);
                if (signin.Succeeded == false)
                {
                    return new ApiErrorResult<string>("Signin Failed");
                }

                var role = await _userManager.GetRolesAsync(user);
                IdentityOptions _options = new IdentityOptions();
                var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub, Constants.AuthorizationConstants.SUBJECT),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
                    new Claim(Constants.UserName, user.UserName),
                    new Claim(_options.ClaimsIdentity.RoleClaimType, role?.FirstOrDefault() ?? Constants.Role.User)
                };

                if (!string.IsNullOrWhiteSpace(user.Email))
                {
                    claims.Append(new Claim(Constants.Email, user.Email));
                }

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Constants.AuthorizationConstants.KEY));
                var signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                var jwtSecurityToken = new JwtSecurityToken(
                    AuthorizationConstants.ISSUER,
                    AuthorizationConstants.AUDIENCE,
                    claims,
                    expires: DateTime.UtcNow.AddMinutes(30),
                    signingCredentials: signIn);

                var token = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);
                return new ApiSuccessResult<string>(token);
            }
            else
            {
                return new ApiErrorResult<string>(Constants.User.Message.LoginIncorrect);
            }
        }

        public ClaimsPrincipal ValidateToken(string jwtToken)
        {
            IdentityModelEventSource.ShowPII = true;

            SecurityToken validatedToken;
            TokenValidationParameters validationParameters = new TokenValidationParameters();

            validationParameters.ValidateLifetime = true;

            validationParameters.ValidAudience = Constants.AuthorizationConstants.AUDIENCE;
            validationParameters.ValidIssuer = Constants.AuthorizationConstants.ISSUER;
            validationParameters.IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Constants.AuthorizationConstants.KEY));

            ClaimsPrincipal principal = new JwtSecurityTokenHandler().ValidateToken(jwtToken, validationParameters, out validatedToken);

            return principal;
        }

        public async Task<ApiResult<bool>> ChangePassword(UserChangePasswordDto dto)
        {
            var user = await _userManager.FindByNameAsync(dto.UserName);
            var result = await _userManager.ChangePasswordAsync(user, dto.OldPassword, dto.NewPassword);
            if (result.Succeeded)
            {
                return new ApiSuccessResult<bool>("Password changed successfully");
            }

            return new ApiErrorResult<bool>(result.Errors.FirstOrDefault().Description);
        }

        public async Task<ApiResult<PagedResult<UserDto>>> GetUsersPaging(GetUserPagingRequest request)
        {
            var users = _userManager.Users.AsQueryable();

            if (!string.IsNullOrWhiteSpace(request.Keyword))
            {
                users = users.Where(x => x.UserName.Contains(request.Keyword));
            }

            int totalRecords = await users.CountAsync();

            var data = await users.Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).ToListAsync();

            var dataResult = _mapper.Map<List<UserDto>>(data);

            var result = new PagedResult<UserDto>()
            {
                PageIndex = request.PageIndex,
                PageSize = request.PageSize,
                TotalRecords = totalRecords,
                Items = dataResult
            };

            return new ApiSuccessResult<PagedResult<UserDto>>(result);
        }
    }
}
