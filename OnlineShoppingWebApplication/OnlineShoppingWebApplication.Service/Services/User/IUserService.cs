﻿using OnlineShoppingWebApplication.Service.Common;
using OnlineShoppingWebApplication.Service.Services.User.DTOs;
using OnlineShoppingWebApplication.ViewModels.Paging;
using OnlineShoppingWebApplication.ViewModels.Paging.User;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace OnlineShoppingWebApplication.Service.Services.User
{
    public interface IUserService
    {
        Task<IList<UserDto>> GetAll();
        Task<ApiResult<string>> Authentication(UserLoginDto dto);
        Task<ApiResult<bool>> Create(UserCreateDto dto);
        Task<ApiResult<bool>> Edit(UserEditDto dto);
        Task<ApiResult<UserDto>> GetByUserName(string userName);
        Task<ApiResult<UserDto>> GetById(Guid id);
        ClaimsPrincipal ValidateToken(string jwtToken);
        Task<ApiResult<bool>> ChangePassword(UserChangePasswordDto dto);
        Task<ApiResult<PagedResult<UserDto>>> GetUsersPaging(GetUserPagingRequest request);
        Task<ApiResult<bool>> DeleteById(Guid id);
    }
}
