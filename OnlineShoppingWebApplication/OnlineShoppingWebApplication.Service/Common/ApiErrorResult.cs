﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace OnlineShoppingWebApplication.Service.Common
{
    public class ApiErrorResult<T> : ApiResult<T>
    {
        private List<IdentityError> identityErrors;

        public string[] ValidationErrors { get; set; }

        public ApiErrorResult() { }

        public ApiErrorResult(string msg)
        {
            IsSuccessed = false;
            Message = msg;
        }

        public ApiErrorResult(string[] validationErrors)
        {
            IsSuccessed = false;
            ValidationErrors = validationErrors;
        }

        public ApiErrorResult(List<IdentityError> identityErrors)
        {
            this.identityErrors = identityErrors;
        }
    }
}
