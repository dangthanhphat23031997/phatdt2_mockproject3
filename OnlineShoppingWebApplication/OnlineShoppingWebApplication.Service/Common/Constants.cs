﻿namespace OnlineShoppingWebApplication.Service.Common
{
    public struct Constants
    {
        public struct ProductImport
        {
            public struct Header
            {
                public const string No = "No";
                public const string ProductName = "Product Name";
                public const string Price = "Price";
                public const string Category = "Category";
                public const string Description = "Description";
                public const string Log = "Log";
            }
        }
        public const string UserName = "UserName";
        public const string Email = "Email";
        public const string Token = "Token";

        public struct Role
        {
            public const string User = "User";
        }

        public struct User
        {
            public struct Message
            {
                public const string LoginIncorrect = "UserName or Password is incorrect";

            }
        }

        public struct AuthorizationConstants
        {
            public const string SUBJECT = "SecretKey";
            public const string KEY = "SecretKeyOfDoomThatMustBeAMinimumNumberOfBytes";
            public const string ISSUER = "SecretKey";
            public const string AUDIENCE = "SecretKey";
        }
    }
}
