﻿namespace OnlineShoppingWebApplication.ViewModels.Paging.Category
{
    public class GetCategoryPagingRequest : PagingRequestBase
    {
        public string Keyword { get; set; }
    }
}
