﻿namespace OnlineShoppingWebApplication.ViewModels.Paging.Product
{
    public class GetProductPagingRequest : PagingRequestBase
    {
        public string Keyword { get; set; }
    }
}
