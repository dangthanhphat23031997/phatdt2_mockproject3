﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using OnlineShoppingWebApplication.Service.Common;
using OnlineShoppingWebApplication.Service.Services;
using OnlineShoppingWebApplication.Service.Services.DTOs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineShoppingWebApplication.ConsoleApp.Manage
{
    public class OnlineShoppingWebApplicationManage
    {
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;
        public OnlineShoppingWebApplicationManage(
            ICategoryService categoryService
            , IProductService productService)
        {
            _categoryService = categoryService;
            _productService = productService;
        }

        #region List Manage
        public async Task ListManage()
        {
            int choose;
            do
            {
                try
                {
                    Console.Clear();
                    Console.WriteLine("============================================");
                    Console.WriteLine("\tOnline Shopping Web Application");
                    Console.WriteLine("============================================");
                    Console.WriteLine("[1] Create Products");
                    Console.WriteLine("[2] Edit Products");
                    Console.WriteLine("[3] Delete Products");
                    Console.WriteLine("[4] Create Category");
                    Console.WriteLine("[5] Edit Category");
                    Console.WriteLine("[6] Delete Category");
                    Console.WriteLine("[7] Import products (Use file)");
                    Console.WriteLine("[8] Search Product");
                    Console.WriteLine("[9] Export");
                    Console.WriteLine("[0] Exit");
                    Console.WriteLine("============================================");
                    Console.Write("Enter a number to choose: ");

                    choose = int.Parse(Console.ReadLine());
                    switch (choose)
                    {
                        case 1:
                            await CreateProduct();
                            break;
                        case 2:
                            await EditProduct();
                            break;
                        case 3:
                            await DeleteProduct();
                            break;
                        case 4:
                            await CreateCategory();
                            break;
                        case 5:
                            await EditCategory();
                            break;
                        case 6:
                            await DeleteCategory();
                            break;
                        case 7:
                            await Import();
                            break;
                        case 8:
                            await SearchProduct();
                            break;
                        case 9:
                            await ExportManage();
                            break;
                        case 0:
                            Environment.Exit(0);
                            break;
                        default:
                            Console.WriteLine("\nInvalid Option: Choose again. Thank you!");
                            Console.WriteLine("==============Enter=To=Continue=============");
                            Console.ReadKey();
                            break;
                    }
                }
                catch
                {
                    Console.WriteLine("\nInvalid Option: Choose again. Thank you!");
                    Console.WriteLine("==============Enter=To=Continue=============");
                    Console.ReadKey();
                }
            } while (true);
        }
        #endregion List Manage

        #region Products

        #region Create
        public async Task CreateProductManage()
        {
            try
            {
                do
                {
                    int choose;
                    Console.WriteLine("[1] To continue");
                    Console.WriteLine("[2] Return");
                    Console.WriteLine("[0] Exit");
                    Console.Write("\nEnter a number to choose: ");
                    choose = int.Parse(Console.ReadLine());
                    switch (choose)
                    {
                        case 1:
                            await CreateProduct();
                            break;
                        case 2:
                            await ListManage();
                            break;
                        case 0:
                            Environment.Exit(0);
                            break;
                        default:
                            Console.WriteLine("\nInvalid Option: Choose again. Thank you!\n");
                            break;
                    }
                }
                while (true);
            }
            catch
            {
                Console.WriteLine("Invalid Option: Choose again. Thank you!\n");
                await CreateProductManage();
                Console.ReadKey();
            }
        }

        public async Task CreateProduct()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("============================================");
                Console.WriteLine("\tOnline Shopping Web Application");
                Console.WriteLine("============================================");
                Console.WriteLine("\nEnter a Product information");
                ProductCreateDto product = new ProductCreateDto();
                do
                {
                    Console.Write("Enter a Name: ");
                    product.Name = Console.ReadLine();
                    if (string.IsNullOrWhiteSpace(product.Name))
                    {
                        Console.WriteLine("[Name] is required!\n");
                    }
                } while (string.IsNullOrWhiteSpace(product.Name));

                bool isPrice;
                do
                {
                    Console.Write("Enter a Price: ");
                    var priceStr = Console.ReadLine();
                    isPrice = decimal.TryParse(priceStr, out decimal price);
                    if (isPrice == true)
                    {
                        product.Price = price;
                    }
                    else
                    {
                        Console.WriteLine($"[Price] is invalid!\n");
                    }
                } while (isPrice == false);

                Guid categoryId = Guid.Empty;
                do
                {
                    Console.Write("Enter a Category: ");
                    var categoryName = Console.ReadLine();
                    if (!string.IsNullOrWhiteSpace(categoryName))
                    {
                        var category = await _categoryService.GetByName(categoryName);
                        if (category.ResultObj == null)
                        {
                            Console.WriteLine($"Cannot found Category [{categoryName}]");
                        }
                        else
                        {
                            categoryId = category.ResultObj.Id;
                        }
                    }
                    else
                    {
                        Console.WriteLine($"[Category] is required!");
                    }
                }
                while (categoryId == Guid.Empty);
                product.CategoryId = categoryId;

                Console.Write("Enter a Description: ");
                product.Description = Console.ReadLine();
                var result = await _productService.Create(product);
                Console.WriteLine($"{result.Message}\n");

                await CreateProductManage();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }
        #endregion

        #region Edit
        public async Task EditProductManage()
        {
            try
            {
                do
                {
                    int choose;
                    Console.WriteLine("[1] To continue");
                    Console.WriteLine("[2] Return");
                    Console.WriteLine("[0] Exit");
                    Console.Write("\nEnter a number to choose: ");
                    choose = int.Parse(Console.ReadLine());
                    switch (choose)
                    {
                        case 1:
                            await EditProduct();
                            break;
                        case 2:
                            await ListManage();
                            break;
                        case 0:
                            Environment.Exit(0);
                            break;
                        default:
                            Console.WriteLine("\nInvalid Option: Choose again. Thank you!\n");
                            break;
                    }
                }
                while (true);
            }
            catch
            {
                Console.WriteLine("Invalid Option: Choose again. Thank you!\n");
                await EditProductManage();
                Console.ReadKey();
            }
        }

        public async Task EditProduct()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("============================================");
                Console.WriteLine("\tOnline Shopping Web Application");
                Console.WriteLine("============================================");

                string name;
                do
                {
                    Console.Write("\nEnter a Product name to edit: ");
                    name = Console.ReadLine();

                    if (string.IsNullOrWhiteSpace(name))
                    {
                        Console.WriteLine("[Name] is invalid!\n");
                    }
                } while (string.IsNullOrWhiteSpace(name));

                var result = await _productService.GetByName(name);
                if (result.IsSuccessed == false)
                {
                    Console.WriteLine($"{result.Message}\n");
                }
                else
                {
                    Console.WriteLine("============Product Information=================");
                    Console.WriteLine($"Name: {result.ResultObj.Name}");
                    Console.WriteLine($"Price: {result.ResultObj.Price}");
                    Console.WriteLine($"Category: {result.ResultObj.Category.Name}");
                    Console.WriteLine($"Description: {result.ResultObj.Description}");
                    Console.WriteLine("\n================Edit====================");
                    do
                    {
                        Console.Write("Enter a Name: ");
                        result.ResultObj.Name = Console.ReadLine();
                        if (string.IsNullOrWhiteSpace(result.ResultObj.Name))
                        {
                            Console.WriteLine("[Name] is required!\n");
                        }
                    } while (string.IsNullOrWhiteSpace(result.ResultObj.Name));

                    bool isPrice;
                    do
                    {
                        Console.Write("Enter a Price: ");
                        var priceStr = Console.ReadLine();
                        isPrice = decimal.TryParse(priceStr, out decimal price);
                        if (isPrice == true)
                        {
                            result.ResultObj.Price = price;
                        }
                        else
                        {
                            Console.WriteLine($"[Price] is invalid!\n");
                        }
                    } while (isPrice == false);

                    Guid categoryId = Guid.Empty;
                    do
                    {
                        Console.Write("Enter a Category: ");
                        var categoryName = Console.ReadLine();
                        if (!string.IsNullOrWhiteSpace(categoryName))
                        {
                            var category = await _categoryService.GetByName(categoryName);
                            if (category.ResultObj == null)
                            {
                                Console.WriteLine($"Cannot found Category [{categoryName}]");
                            }
                            else
                            {
                                categoryId = category.ResultObj.Id;
                            }
                        }
                        else
                        {
                            Console.WriteLine($"[Category] is required!");
                        }
                    }
                    while (categoryId == Guid.Empty);
                    result.ResultObj.CategoryId = categoryId;

                    Console.Write("Enter a Description: ");
                    result.ResultObj.Description = Console.ReadLine();
                    var editResult = await _productService.Edit(result.ResultObj);
                    Console.WriteLine($"{editResult.Message}\n");
                }

                await EditProductManage();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }
        #endregion

        #region Delete
        public async Task DeleteProductManage()
        {
            try
            {
                do
                {
                    int choose;
                    Console.WriteLine("[1] To continue");
                    Console.WriteLine("[2] Return");
                    Console.WriteLine("[0] Exit");
                    Console.Write("\nEnter a number to choose: ");
                    choose = int.Parse(Console.ReadLine());
                    switch (choose)
                    {
                        case 1:
                            await DeleteProduct();
                            break;
                        case 2:
                            await ListManage();
                            break;
                        case 0:
                            Environment.Exit(0);
                            break;
                        default:
                            Console.WriteLine("\nInvalid Option: Choose again. Thank you!\n");
                            break;
                    }
                }
                while (true);
            }
            catch
            {
                Console.WriteLine("Invalid Option: Choose again. Thank you!\n");
                await DeleteProductManage();
                Console.ReadKey();
            }
        }

        public async Task DeleteProduct()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("============================================");
                Console.WriteLine("\tOnline Shopping Web Application");
                Console.WriteLine("============================================");

                string name;
                do
                {
                    Console.Write("\nEnter a Product name to delete: ");
                    name = Console.ReadLine();

                    if (string.IsNullOrWhiteSpace(name))
                    {
                        Console.WriteLine("[Name] is invalid!\n");
                    }
                } while (string.IsNullOrWhiteSpace(name));

                var result = await _productService.DeleteByName(name);
                Console.WriteLine($"{result.Message}\n");

                await DeleteProductManage();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }
        #endregion

        #region Search
        public async Task SearchProductManage()
        {
            try
            {
                do
                {
                    int choose;
                    Console.WriteLine("[1] To continue");
                    Console.WriteLine("[2] Return");
                    Console.WriteLine("[0] Exit");
                    Console.Write("\nEnter a number to choose: ");
                    choose = int.Parse(Console.ReadLine());
                    switch (choose)
                    {
                        case 1:
                            await SearchProduct();
                            break;
                        case 2:
                            await ListManage();
                            break;
                        case 0:
                            Environment.Exit(0);
                            break;
                        default:
                            Console.WriteLine("\nInvalid Option: Choose again. Thank you!\n");
                            break;
                    }
                }
                while (true);
            }
            catch
            {
                Console.WriteLine("Invalid Option: Choose again. Thank you!\n");
                await SearchProductManage();
                Console.ReadKey();
            }
        }

        public async Task SearchProduct()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("============================================");
                Console.WriteLine("\tOnline Shopping Web Application");
                Console.WriteLine("============================================");

                string name;
                do
                {
                    Console.Write("\nEnter a Product name want to find: ");
                    name = Console.ReadLine();

                    if (string.IsNullOrWhiteSpace(name))
                    {
                        Console.WriteLine("[Name] is invalid!\n");
                    }
                } while (string.IsNullOrWhiteSpace(name));

                var result = await _productService.GetListByName(name);

                Console.WriteLine("\n=================List=All=Products======================");
                if (result.IsSuccessed == true)
                {
                    foreach(var item in result.ResultObj)
                    {
                        Console.WriteLine($"Name: {item.Name}");
                        Console.WriteLine($"Price: {item.Price}");
                        Console.WriteLine($"Category: {item.Category.Name}");
                        Console.WriteLine($"Description: {item.Description}");
                        Console.WriteLine("--------------------------------------------");
                    }
                }
                else
                {
                    Console.WriteLine($"{result.Message}\n");
                }
                Console.WriteLine("\n=================Exit======================\n");
               
                await SearchProductManage();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }
        #endregion

        #endregion

        #region Category
        #region Create
        public async Task CreateCategoryManage()
        {
            try
            {
                do
                {
                    int choose;
                    Console.WriteLine("[1] To continue");
                    Console.WriteLine("[2] Return");
                    Console.WriteLine("[0] Exit");
                    Console.Write("\nEnter a number to choose: ");
                    choose = int.Parse(Console.ReadLine());
                    switch (choose)
                    {
                        case 1:
                            await CreateCategory();
                            break;
                        case 2:
                            await ListManage();
                            break;
                        case 0:
                            Environment.Exit(0);
                            break;
                        default:
                            Console.WriteLine("\nInvalid Option: Choose again. Thank you!\n");
                            break;
                    }
                }
                while (true);
            }
            catch
            {
                Console.WriteLine("Invalid Option: Choose again. Thank you!\n");
                await CreateCategoryManage();
                Console.ReadKey();
            }
        }

        public async Task CreateCategory()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("============================================");
                Console.WriteLine("\tOnline Shopping Web Application");
                Console.WriteLine("============================================");
                Console.WriteLine("\nEnter a Category information");
                CategoryCreateDto category = new CategoryCreateDto();
                do
                {
                    Console.Write("Enter a Name: ");
                    category.Name = Console.ReadLine();
                    if (string.IsNullOrWhiteSpace(category.Name))
                    {
                        Console.WriteLine("[Name] is required!\n");
                    }
                } while (string.IsNullOrWhiteSpace(category.Name));

                Console.Write("Enter a Description: ");
                category.Description = Console.ReadLine();
                var result = await _categoryService.Create(category);
                Console.WriteLine($"{result.Message}\n");

                await CreateCategoryManage();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }
        #endregion

        #region Edit
        public async Task EditCategoryManage()
        {
            try
            {
                do
                {
                    int choose;
                    Console.WriteLine("[1] To continue");
                    Console.WriteLine("[2] Return");
                    Console.WriteLine("[0] Exit");
                    Console.Write("\nEnter a number to choose: ");
                    choose = int.Parse(Console.ReadLine());
                    switch (choose)
                    {
                        case 1:
                            await EditCategory();
                            break;
                        case 2:
                            await ListManage();
                            break;
                        case 0:
                            Environment.Exit(0);
                            break;
                        default:
                            Console.WriteLine("\nInvalid Option: Choose again. Thank you!\n");
                            break;
                    }
                }
                while (true);
            }
            catch
            {
                Console.WriteLine("Invalid Option: Choose again. Thank you!\n");
                await EditCategoryManage();
                Console.ReadKey();
            }
        }

        public async Task EditCategory()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("============================================");
                Console.WriteLine("\tOnline Shopping Web Application");
                Console.WriteLine("============================================");

                string name;
                do
                {
                    Console.Write("\nEnter a Category name to edit: ");
                    name = Console.ReadLine();

                    if (string.IsNullOrWhiteSpace(name))
                    {
                        Console.WriteLine("[Name] is invalid!\n");
                    }
                } while (string.IsNullOrWhiteSpace(name));

                var result = await _categoryService.GetByName(name);
                if (result.IsSuccessed == false)
                {
                    Console.WriteLine($"{result.Message}\n");
                }
                else
                {
                    Console.WriteLine("============Category Information=================");
                    Console.WriteLine($"Name: {result.ResultObj.Name}");
                    Console.WriteLine($"Description: {result.ResultObj.Description}");
                    Console.WriteLine("\n================Edit====================");
                    do
                    {
                        Console.Write("Enter a Name: ");
                        result.ResultObj.Name = Console.ReadLine();
                        if (string.IsNullOrWhiteSpace(result.ResultObj.Name))
                        {
                            Console.WriteLine("[Name] is required!\n");
                        }
                    } while (string.IsNullOrWhiteSpace(result.ResultObj.Name));

                    Console.Write("Enter a Description: ");
                    result.ResultObj.Description = Console.ReadLine();
                    var editResult = await _categoryService.Edit(result.ResultObj);
                    Console.WriteLine($"{editResult.Message}\n");
                }

                await EditCategoryManage();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }
        #endregion

        #region Delete
        public async Task DeleteCategoryManage()
        {
            try
            {
                do
                {
                    int choose;
                    Console.WriteLine("[1] To continue");
                    Console.WriteLine("[2] Return");
                    Console.WriteLine("[0] Exit");
                    Console.Write("\nEnter a number to choose: ");
                    choose = int.Parse(Console.ReadLine());
                    switch (choose)
                    {
                        case 1:
                            await DeleteCategory();
                            break;
                        case 2:
                            await ListManage();
                            break;
                        case 0:
                            Environment.Exit(0);
                            break;
                        default:
                            Console.WriteLine("\nInvalid Option: Choose again. Thank you!\n");
                            break;
                    }
                }
                while (true);
            }
            catch
            {
                Console.WriteLine("Invalid Option: Choose again. Thank you!\n");
                await DeleteCategoryManage();
                Console.ReadKey();
            }
        }

        public async Task DeleteCategory()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("============================================");
                Console.WriteLine("\tOnline Shopping Web Application");
                Console.WriteLine("============================================");

                string name;
                do
                {
                    Console.Write("\nEnter a Category name to delete: ");
                    name = Console.ReadLine();

                    if (string.IsNullOrWhiteSpace(name))
                    {
                        Console.WriteLine("[Name] is invalid!\n");
                    }
                } while (string.IsNullOrWhiteSpace(name));

                var result = await _categoryService.DeleteByName(name);
                Console.WriteLine($"{result.Message}\n");

                await DeleteCategoryManage();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }
        #endregion

        #endregion

        #region Export
        public async Task ExportManage()
        {
            int choose;
            do
            {
                try
                {
                    Console.Clear();
                    Console.WriteLine("============================================");
                    Console.WriteLine("\tOnline Shopping Web Application");
                    Console.WriteLine("============================================");
                    Console.WriteLine("[1] View all products");
                    Console.WriteLine("[2] List products by Category");
                    Console.WriteLine("[3] Export file");
                    Console.WriteLine("[4] Return");
                    Console.WriteLine("[0] Exit");
                    Console.WriteLine("============================================");
                    Console.Write("Enter a number to choose: ");
                    choose = int.Parse(Console.ReadLine());

                    switch (choose)
                    {
                        case 1:
                            await ListAllProducts();
                            break;
                        case 2:
                            await ListProductsByCategory();
                            break;
                        case 3:
                            await Export();
                            break;
                        case 4:
                            await ListManage();
                            break;
                        case 0:
                            Environment.Exit(0);
                            break;
                        default:
                            Console.WriteLine("\nInvalid Option: Choose again. Thank you!");
                            Console.WriteLine("==============Enter=To=Continue=============");
                            Console.ReadKey();
                            break;
                    }
                }
                catch
                {
                    Console.WriteLine("\nInvalid Option: Choose again. Thank you!");
                    Console.WriteLine("==============Enter=To=Continue=============");
                    Console.ReadKey();
                }
            }while(true);
        }

        #region List All Products
        public async Task ListAllProducts()
        {
            var products = await _productService.GetAllIncludeCategory();
            Console.WriteLine("\n=================List=All=Products======================");
            if (products == null)
            {
                Console.WriteLine("Empty");
            }
            else
            {
                foreach (var item in products)
                {
                    Console.WriteLine($"Name: {item.Name}");
                    Console.WriteLine($"Price: {item.Price}");
                    Console.WriteLine($"Category: {item.Category.Name}");
                    Console.WriteLine($"Description: {item.Description}");
                    Console.WriteLine("--------------------------------------------");
                }
                await ExportCommon(products);
            }

            Console.WriteLine("==============Enter=To=Continue=============");
            Console.ReadKey();
        }
        #endregion

        #region List Products By Category
        public async Task ListProductsByCategory()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("============================================");
                Console.WriteLine("\tOnline Shopping Web Application");
                Console.WriteLine("============================================");

                string name;
                do
                {
                    Console.Write("\nEnter a Category name: ");
                    name = Console.ReadLine();

                    if (string.IsNullOrWhiteSpace(name))
                    {
                        Console.WriteLine("[Name] is invalid!\n");
                    }
                } while (string.IsNullOrWhiteSpace(name));

                var categoryResult = await _categoryService.GetByName(name);

                Console.WriteLine($"\n=================List=All=Products=By=Category=[{name}]===================");
                if (categoryResult.IsSuccessed == true)
                {
                    var products = await _productService.GetListByCategoryId(categoryResult.ResultObj.Id);
                    foreach (var item in products)
                    {
                        Console.WriteLine($"Name: {item.Name}");
                        Console.WriteLine($"Price: {item.Price}");
                        Console.WriteLine($"Category: {item.Category.Name}");
                        Console.WriteLine($"Description: {item.Description}");
                        Console.WriteLine("--------------------------------------------");
                    }
                    await ExportCommon(products);
                }
                else
                {
                    Console.WriteLine($"{categoryResult.Message}\n");
                    Console.WriteLine("==============Enter=To=Continue=============");
                    Console.ReadKey();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }
        #endregion

        #region Export
        public async Task Export()
        {
            var products = await _productService.GetAllIncludeCategory();
            if(products != null && products.Count > 0)
            {
                WritingToExcel(products);
            }
        }
        #endregion

        #region Export Common
        public async Task ExportCommon(IList<ProductDto> productList)
        {
            Console.WriteLine("Are you want to export?");
            Console.WriteLine("[1] Yes");
            Console.WriteLine("[2] No");

            do
            {
                try
                {
                    int choose = 0;
                    Console.Write("Enter a number to choose: ");
                    choose = int.Parse(Console.ReadLine());
                    switch (choose)
                    {
                        case 1:
                            WritingToExcel(productList);
                            await ExportManage();
                            break;
                        case 2:
                            await ExportManage();
                            return;
                        default:
                            Console.WriteLine("\nInvalid Option: Choose again. Thank you!");
                            break;
                    }
                }
                catch
                {
                    Console.WriteLine("\nInvalid Option: Choose again. Thank you!");
                }
            } while (true);
        }
        #endregion

        #region Writting To Excel
        public void WritingToExcel(IList<ProductDto> productList)
        {
            ExcelPackage excel = new ExcelPackage();

            var workSheet = excel.Workbook.Worksheets.Add("Product");

            workSheet.TabColor = System.Drawing.Color.Black;
            workSheet.DefaultRowHeight = 12;

            workSheet.Row(1).Height = 20;
            workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Row(1).Style.Font.Bold = true;

            // Header of the Excel sheet
            workSheet.Cells[1, 1].Value = Constants.ProductImport.Header.No;
            workSheet.Cells[1, 2].Value = Constants.ProductImport.Header.ProductName;
            workSheet.Cells[1, 3].Value = Constants.ProductImport.Header.Price;
            workSheet.Cells[1, 4].Value = Constants.ProductImport.Header.Category;
            workSheet.Cells[1, 5].Value = Constants.ProductImport.Header.Description;

            int recordIndex = 2;

            foreach (var product in productList)
            {
                workSheet.Cells[recordIndex, 1].Value = (recordIndex - 1).ToString();
                workSheet.Cells[recordIndex, 2].Value = product.Name;
                workSheet.Cells[recordIndex, 3].Value = product.Price;
                workSheet.Cells[recordIndex, 4].Value = product.Category.Name;
                workSheet.Cells[recordIndex, 5].Value = product.Description;
                recordIndex++;
            }

            workSheet.Columns.AutoFit();

            string p_strPath = "Product.xlsx";

            if (File.Exists(p_strPath))
                File.Delete(p_strPath);

            // Create excel file on physical disk 
            FileStream objFileStrm = File.Create(p_strPath);
            objFileStrm.Close();

            // Write content to excel file 
            File.WriteAllBytes(p_strPath, excel.GetAsByteArray());
            //Close Excel package
            excel.Dispose();
            Console.WriteLine($"\nExport to excel success!");
            Console.WriteLine($"Path: {objFileStrm.Name}");
            Console.WriteLine("==============Enter=To=Continue=============");
            Console.ReadKey();
        }
        #endregion
        #endregion

        #region Import
        public async Task ImportManage()
        {
            try
            {
                do
                {
                    int choose;
                    Console.WriteLine("[1] To continue");
                    Console.WriteLine("[2] Return");
                    Console.WriteLine("[0] Exit");
                    Console.Write("\nEnter a number to choose: ");
                    choose = int.Parse(Console.ReadLine());
                    switch (choose)
                    {
                        case 1:
                            await Import();
                            break;
                        case 2:
                            await ListManage();
                            break;
                        case 0:
                            Environment.Exit(0);
                            break;
                        default:
                            Console.WriteLine("\nInvalid Option: Choose again. Thank you!\n");
                            break;
                    }
                }
                while (true);
            }
            catch
            {
                Console.WriteLine("Invalid Option: Choose again. Thank you!\n");
                await CreateCategoryManage();
                Console.ReadKey();
            }
        }


        public async Task Import()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("============================================");
                Console.WriteLine("\tOnline Shopping Web Application");
                Console.WriteLine("============================================");

                string path;

                do
                {
                    Console.Write("\nEnter a file(Excel) path to import: ");
                    path = Console.ReadLine();

                    if (string.IsNullOrWhiteSpace(path))
                    {
                        Console.WriteLine("[Path] is invalid!\n");
                    }
                } while (string.IsNullOrWhiteSpace(path));
                //F:\F_Soft\EnrichSkill\Project_1\Project\MockProject1_Final\Product_ImportTamplate.xlsx
                var isExistsfilePath = File.Exists(path);
                if(isExistsfilePath == true)
                {
                    FileInfo fileInfo = new FileInfo(path);
                    ExcelPackage package = new ExcelPackage(fileInfo);
                    ExcelWorksheet worksheet = package.Workbook.Worksheets["Product"];
                    if(worksheet == null)
                    {
                        Console.WriteLine($"Import Product Template is invalid!");
                        Console.WriteLine("==============Enter=To=Continue=============");
                        Console.ReadKey();
                        await ImportManage();
                    }

                    int rows = worksheet?.Dimension?.Rows ?? 0;
                    int columns = worksheet?.Dimension?.Columns ?? 0;

                    #region Check header
                    bool inValidHeader = false;
                    if(rows == 0 && columns == 0)
                    {
                        inValidHeader = true;
                    }
                    else if (worksheet.Cells[1, 1].Value?.ToString().Equals(Constants.ProductImport.Header.No, StringComparison.OrdinalIgnoreCase) == false)
                    {
                        inValidHeader = true;
                    }
                    else if (worksheet.Cells[1, 2].Value?.ToString().Equals(Constants.ProductImport.Header.ProductName, StringComparison.OrdinalIgnoreCase) == false)
                    {
                        inValidHeader = true;
                    }
                    else if (worksheet.Cells[1, 3].Value?.ToString().Equals(Constants.ProductImport.Header.Price, StringComparison.OrdinalIgnoreCase) == false)
                    {
                        inValidHeader = true;
                    }
                    else if (worksheet.Cells[1, 4].Value?.ToString().Equals(Constants.ProductImport.Header.Category, StringComparison.OrdinalIgnoreCase) == false)
                    {
                        inValidHeader = true;
                    }
                    else if (worksheet.Cells[1, 5].Value?.ToString().Equals(Constants.ProductImport.Header.Description, StringComparison.OrdinalIgnoreCase) == false)
                    {
                        inValidHeader = true;
                    }

                    if (inValidHeader == true)
                    {
                        Console.WriteLine($"Import Product Template is invalid!");
                        Console.WriteLine("==============Enter=To=Continue=============");
                        Console.ReadKey();
                        await ImportManage();
                    }
                    #endregion

                    var categoryList = await _categoryService.GetAll();
                    IList<ProductImportDto> productListImport = new List<ProductImportDto>();
                    for (int row = 2; row <= rows; row++)
                    {
                        var cellRange = worksheet.Cells[row, 1, row, columns];
                        var isEmpty = cellRange.All(x => x.Value == null);
                        if(isEmpty == true)
                        {
                            continue;
                        }
                        ProductImportDto product = new ProductImportDto();
                        product.Name = worksheet.Cells[row, 2].Value?.ToString();
                        try
                        {
                            product.Price = decimal.Parse(worksheet.Cells[row, 3].Value?.ToString());
                        }
                        catch
                        {
                            product.Price = null;
                        }
                        product.CategoryName = worksheet.Cells[row, 4].Value?.ToString();
                        product.Description = worksheet.Cells[row, 5].Value?.ToString();
                        var category = categoryList.FirstOrDefault(x => x.Name == product.CategoryName);
                        if (string.IsNullOrWhiteSpace(product.Name))
                        {
                            product.Log = "[Product Name] is required!";
                        }
                        else if(product.Price == null)
                        {
                            product.Log = "[Price] is required or invalid!";
                        }
                        else if(category == null)
                        {
                            product.Log = "[Category] is required or invalid";
                        }
                        else if(category != null)
                        {
                            product.CategoryId = category.Id;
                        }
                        productListImport.Add(product);
                    }
                    if(productListImport.Count < 1)
                    {
                        Console.WriteLine($"Import Product Template is empty!");
                        Console.WriteLine("==============Enter=To=Continue=============");
                        Console.ReadKey();
                        await ImportManage();
                    }
                    else
                    {
                        var productsImportValid = productListImport.Where(x => string.IsNullOrEmpty(x.Log)).ToList();
                        var result = await _productService.AddRange(productsImportValid);
                        Console.WriteLine(result.Message);
                        WritingLogToExcel(productListImport, result.IsSuccessed);
                    }
                }
                else
                {
                    Console.WriteLine($"Cannot found file path: ${path}");
                    Console.WriteLine("==============Enter=To=Continue=============");
                    Console.ReadKey();
                    await ImportManage();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        #region Writting To Excel
        public void WritingLogToExcel(IList<ProductImportDto> productImports, bool isInsert)
        {
            ExcelPackage excel = new ExcelPackage();

            var workSheet = excel.Workbook.Worksheets.Add("Products_Import_Result_Log");

            workSheet.TabColor = System.Drawing.Color.Black;
            workSheet.DefaultRowHeight = 12;

            workSheet.Row(1).Height = 20;
            workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Row(1).Style.Font.Bold = true;

            // Header of the Excel sheet
            workSheet.Cells[1, 1].Value = Constants.ProductImport.Header.No;
            workSheet.Cells[1, 2].Value = Constants.ProductImport.Header.ProductName;
            workSheet.Cells[1, 3].Value = Constants.ProductImport.Header.Price;
            workSheet.Cells[1, 4].Value = Constants.ProductImport.Header.Category;
            workSheet.Cells[1, 5].Value = Constants.ProductImport.Header.Description;
            workSheet.Cells[1, 6].Value = Constants.ProductImport.Header.Log;

            int recordIndex = 2;

            foreach (var product in productImports)
            {
                workSheet.Cells[recordIndex, 1].Value = (recordIndex - 1).ToString();
                workSheet.Cells[recordIndex, 2].Value = product.Name;
                workSheet.Cells[recordIndex, 3].Value = product.Price;
                workSheet.Cells[recordIndex, 4].Value = product.CategoryName;
                workSheet.Cells[recordIndex, 5].Value = product.Description;
                if (string.IsNullOrWhiteSpace(product.Log))
                {
                    workSheet.Cells[recordIndex, 6].Value = isInsert == true ? "Import Success" : "Import Failed";
                }
                else
                {
                    workSheet.Cells[recordIndex, 6].Value = product.Log;
                }
                recordIndex++;
            }

            workSheet.Columns.AutoFit();

            string p_strPath = "Products_Import_Result_Log.xlsx";

            if (File.Exists(p_strPath))
                File.Delete(p_strPath);

            // Create excel file on physical disk 
            FileStream objFileStrm = File.Create(p_strPath);
            objFileStrm.Close();

            // Write content to excel file 
            File.WriteAllBytes(p_strPath, excel.GetAsByteArray());
            //Close Excel package
            excel.Dispose();
            Console.WriteLine($"\nImport Product Result:");
            Console.WriteLine($"Path: {objFileStrm.Name}");
            Console.WriteLine("==============Enter=To=Continue=============");
            Console.ReadKey();
        }
        #endregion

        #endregion
    }
}
